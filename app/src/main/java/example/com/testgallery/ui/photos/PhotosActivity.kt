package example.com.testgallery.ui.photos

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import example.com.testgallery.R
import example.com.testgallery.databinding.ActivityPhotosBinding
import example.com.testgallery.ui.base.BaseActivity
import org.koin.android.ext.android.inject

class PhotosActivity : BaseActivity() {
    override val contextName = "PhotosActivity"

    private val viewModelFactory by inject<PhotosViewModelFactory>()
    private val adapter by inject<PhotosAdapter>()

    private lateinit var binding: ActivityPhotosBinding

    companion object {
        val PHOTO_ID = "AlbumsActivity.USER_ID"
        val PHOTO_TITLE = "AlbumActivity.USER_NAME"

        fun newIntentInstance(context: Context, photoId: Int, title: String): Intent {
            val intent = Intent(context, PhotosActivity::class.java)
            intent.putExtra(PhotosActivity.PHOTO_ID, photoId)
            intent.putExtra(PhotosActivity.PHOTO_TITLE, title)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_photos)
        binding.apply {
            viewModel = ViewModelProviders.of(this@PhotosActivity, viewModelFactory).get(PhotosViewModel::class.java)
            executePendingBindings()
        }
        if (intent.hasExtra(PHOTO_ID)) {
            binding.viewModel?.albumId = intent.getIntExtra(PHOTO_ID, 0)
        }
        if (intent.hasExtra(PHOTO_TITLE)) {
            title = intent.getStringExtra(PHOTO_TITLE)
        }
        setupViews()
    }

    override fun onStart() {
        super.onStart()
        binding.viewModel?.getPhotos()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupViews() {
        binding.apply {
            recycler.layoutManager = LinearLayoutManager(this@PhotosActivity)
            recycler.adapter = adapter
            recycler.itemAnimator = DefaultItemAnimator()
            viewModel?.photos?.observe(this@PhotosActivity, Observer { data ->
                adapter.items = data.orEmpty()
            })
        }
    }
}
