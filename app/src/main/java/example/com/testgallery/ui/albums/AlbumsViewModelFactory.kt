package example.com.testgallery.ui.albums

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import example.com.testgallery.App
import example.com.testgallery.data.repositories.UsersRepository

class AlbumsViewModelFactory(private val app: App,
                             private val usersRepository: UsersRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AlbumsViewModel::class.java)) {
            return AlbumsViewModel(app, usersRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}