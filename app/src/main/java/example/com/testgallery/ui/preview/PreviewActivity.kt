package example.com.testgallery.ui.preview

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.github.chrisbanes.photoview.PhotoView
import com.squareup.picasso.Picasso
import example.com.testgallery.ui.base.BaseActivity
import example.com.testgallery.R

class PreviewActivity : BaseActivity() {
    override val contextName: String = "PreviewActivity"

    companion object {
        val IMAGE_URL = "PreviewActivity.IMAGE_URL"

        fun newIntentInstance(context: Context, url: String): Intent {
            val intent = Intent(context, PreviewActivity::class.java)
            intent.putExtra(IMAGE_URL, url)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_preview)

        val url = intent.getStringExtra(IMAGE_URL)
        Picasso.with(this)
                .load(url)
                .placeholder(R.drawable.downloading)
                .into(findViewById<PhotoView>(R.id.preview))
    }
}