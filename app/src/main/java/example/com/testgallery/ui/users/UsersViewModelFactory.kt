package example.com.testgallery.ui.users

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import example.com.testgallery.App
import example.com.testgallery.data.repositories.UsersRepository

class UsersViewModelFactory(private val app: App, private val usersRepository: UsersRepository): ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UsersViewModel::class.java)) {
            return UsersViewModel(app, usersRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}