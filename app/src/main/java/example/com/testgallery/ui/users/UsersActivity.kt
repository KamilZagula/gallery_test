package example.com.testgallery.ui.users

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import example.com.testgallery.R
import example.com.testgallery.databinding.ActivityUsersBinding
import example.com.testgallery.ui.base.BaseActivity
import org.koin.android.ext.android.inject

class UsersActivity : BaseActivity() {

    override val contextName: String = "UsersActivity"

    private val viewModelFactory by inject<UsersViewModelFactory>()
    private val adapter by inject<UsersAdapter>()

    private lateinit var binding: ActivityUsersBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_users)
        binding.apply {
            viewModel = ViewModelProviders.of(this@UsersActivity, viewModelFactory).get(UsersViewModel::class.java)
            executePendingBindings()
        }
        setupView()
    }

    private fun setupView() {
        binding.apply {
            recycler.layoutManager = LinearLayoutManager(this@UsersActivity)
            recycler.adapter = this@UsersActivity.adapter
            recycler.itemAnimator = DefaultItemAnimator()
            viewModel?.users?.observe(this@UsersActivity, Observer { data ->
                adapter.items = data.orEmpty()
            })
        }
    }

    override fun onStart() {
        super.onStart()
        binding.viewModel?.getUsers()
    }
}