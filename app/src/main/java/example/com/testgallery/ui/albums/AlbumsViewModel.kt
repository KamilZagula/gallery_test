package example.com.testgallery.ui.albums

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import example.com.testgallery.App
import example.com.testgallery.data.models.Album
import example.com.testgallery.data.repositories.UsersRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class AlbumsViewModel(app: App, private val usersRepository: UsersRepository) : AndroidViewModel(app) {

    val isLoading = ObservableField(false)
    val albums = MutableLiveData<List<Album>>()
    var userId: Int = 0
    private val disposables = CompositeDisposable()


    fun getAlbums() {
        isLoading.set(true)
        disposables.add(usersRepository.getUserAlbums(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onError = { error ->
                    Timber.e(error, "Error while getting albums")
                    isLoading.set(false)
                }, onSuccess = { albums ->
                    isLoading.set(false)
                    this.albums.value = albums
                }))
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}