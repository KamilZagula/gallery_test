package example.com.testgallery.ui.albums

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import example.com.testgallery.R
import example.com.testgallery.databinding.ActivityAlbumsBinding
import example.com.testgallery.ui.base.BaseActivity
import org.koin.android.ext.android.inject

class AlbumsActivity : BaseActivity() {
    override val contextName = "AlbumsActivity"

    private val viewModelFactory by inject<AlbumsViewModelFactory>()
    private val adapter by inject<AlbumsAdapter>()

    private lateinit var binding: ActivityAlbumsBinding

    companion object {
        val USER_EXTRA = "AlbumsActivity.USER_EXTRA"
        val USER_NAME = "AlbumActivity.USER_NAME"

        fun newIntentInstance(context: Context, userId: Int, userName: String): Intent {
            val intent = Intent(context, AlbumsActivity::class.java)
            intent.putExtra(AlbumsActivity.USER_EXTRA, userId)
            intent.putExtra(AlbumsActivity.USER_NAME, userName)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_albums)
        binding.apply {
            viewModel = ViewModelProviders.of(this@AlbumsActivity, viewModelFactory).get(AlbumsViewModel::class.java)
            executePendingBindings()
        }
        if (intent.hasExtra(AlbumsActivity.USER_EXTRA)) {
            binding.viewModel?.userId = intent.getIntExtra(AlbumsActivity.USER_EXTRA, 0)
        }
        if (intent.hasExtra(AlbumsActivity.USER_NAME)) {
            title = getString(R.string.users_albums, intent.getStringExtra(AlbumsActivity.USER_NAME))
        }
        setupViews()
    }

    override fun onStart() {
        super.onStart()
        binding.viewModel?.getAlbums()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupViews() {
        binding.apply {
            recycler.layoutManager = LinearLayoutManager(this@AlbumsActivity)
            recycler.adapter = adapter
            recycler.itemAnimator = DefaultItemAnimator()
            viewModel?.albums?.observe(this@AlbumsActivity, Observer { data ->
                adapter.items = data.orEmpty()
            })
        }
    }
}