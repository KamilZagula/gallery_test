package example.com.testgallery.ui.base

import android.support.v7.app.AppCompatActivity
import org.koin.standalone.releaseContext

abstract class BaseActivity: AppCompatActivity() {
    abstract val contextName: String

    override fun onPause() {
        releaseContext(contextName)
        super.onPause()
    }
}