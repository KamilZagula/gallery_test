package example.com.testgallery.ui.albums

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import example.com.testgallery.data.models.Album
import example.com.testgallery.databinding.ItemAlbumBinding
import example.com.testgallery.ui.base.BaseListAdapter
import example.com.testgallery.ui.photos.PhotosActivity

class AlbumsAdapter(context: Context) : BaseListAdapter<Album, ItemAlbumBinding, AlbumsAdapter.ViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val binding = ItemAlbumBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(context, binding)
    }

    class ViewHolder(context: Context, binding: ItemAlbumBinding) : BaseListAdapter.ViewHolder<ItemAlbumBinding, Album>(context, binding) {

        override fun bind(item: Album) {
            binding.album = item
            binding.root.setOnClickListener {
                context.startActivity(PhotosActivity.newIntentInstance(context, item.id, item.title))
            }
            binding.executePendingBindings()
        }
    }
}