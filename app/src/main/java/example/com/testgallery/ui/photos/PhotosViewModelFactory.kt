package example.com.testgallery.ui.photos

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import example.com.testgallery.App
import example.com.testgallery.data.repositories.PhotosRepository

class PhotosViewModelFactory(private val app: App,
                             private val photosRepository: PhotosRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PhotosViewModel::class.java)) {
            return PhotosViewModel(app, photosRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}