package example.com.testgallery.ui.photos

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import example.com.testgallery.R
import example.com.testgallery.data.models.Photo
import example.com.testgallery.databinding.ItemPhotoBinding
import example.com.testgallery.ui.base.BaseListAdapter
import example.com.testgallery.ui.preview.PreviewActivity

class PhotosAdapter(context: Context) : BaseListAdapter<Photo, ItemPhotoBinding, PhotosAdapter.ViewHolder>(context) {


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val binding = ItemPhotoBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(context, binding)
    }


    class ViewHolder(context: Context, binding: ItemPhotoBinding) :
            BaseListAdapter.ViewHolder<ItemPhotoBinding, Photo>(context, binding) {

        override fun bind(item: Photo) {
            binding.photo = item
            Picasso.with(context)
                    .load(item.thumbnailUrl)
                    .placeholder(R.drawable.default_placeholder)
                    .error(R.drawable.default_placeholder)
                    .into(binding.iconPhotoAlbum)

            binding.root.setOnClickListener {
                context.startActivity(PreviewActivity.newIntentInstance(context, item.url))
            }
            binding.executePendingBindings()
        }

    }
}