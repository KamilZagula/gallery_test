package example.com.testgallery.ui.base

import android.content.Context
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

/**
 * BaseListAdapter for RecyclerView.
 * @param M model
 * @param B ViewBinding
 * @param VH ViewHolder that extends [BaseListAdapter.ViewHolder]
 * @param context Context
 */
abstract class BaseListAdapter<M, out B : ViewDataBinding, VH : BaseListAdapter.ViewHolder<B, M>>(protected val context: Context) : RecyclerView.Adapter<VH>() {

    var items: List<M> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: VH, position: Int) = holder.bind(items[position])

    /**
     * View holder for [BaseListAdapter]
     * @param T ViewBinding
     * @param M model
     * @param context Context
     * @param binding instance of ViewBinding
     */
    abstract class ViewHolder<out T : ViewDataBinding, in M>(protected val context: Context, protected val binding: T) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Setup bindings here.
         */
        abstract fun bind(item: M)
    }

}