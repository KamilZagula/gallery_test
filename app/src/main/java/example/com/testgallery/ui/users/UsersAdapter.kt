package example.com.testgallery.ui.users

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import example.com.testgallery.data.models.User
import example.com.testgallery.databinding.ItemUserBinding
import example.com.testgallery.ui.albums.AlbumsActivity
import example.com.testgallery.ui.base.BaseListAdapter

class UsersAdapter(context: Context) : BaseListAdapter<User, ItemUserBinding, UsersAdapter.ViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val binding = ItemUserBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(context, binding)
    }


    class ViewHolder(context: Context, binding: ItemUserBinding) : BaseListAdapter.ViewHolder<ItemUserBinding, User>(context, binding) {
        override fun bind(item: User) {
            binding.user = item
            binding.root.setOnClickListener {
                context.startActivity(AlbumsActivity.newIntentInstance(context, item.id, item.name))
            }
            binding.executePendingBindings()
        }
    }
}
