package example.com.testgallery.ui.photos

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import example.com.testgallery.App
import example.com.testgallery.data.models.Photo
import example.com.testgallery.data.repositories.PhotosRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class PhotosViewModel(app: App, private val photosRepository: PhotosRepository) : AndroidViewModel(app) {

    val isLoading = ObservableField(false)
    val photos = MutableLiveData<List<Photo>>()
    var albumId: Int = 0
    private val disposables = CompositeDisposable()


    fun getPhotos() {
        isLoading.set(true)
        disposables.add(photosRepository.getPhotosFromAlbum(albumId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onError = { error ->
                    Timber.e(error, "Error while getting albums")
                    isLoading.set(false)
                }, onSuccess = { albums ->
                    isLoading.set(false)
                    this.photos.value = albums
                }))
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}