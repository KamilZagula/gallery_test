package example.com.testgallery.ui.users

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import example.com.testgallery.App
import example.com.testgallery.data.models.User
import example.com.testgallery.data.repositories.UsersRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class UsersViewModel(app: App, private val usersRepository: UsersRepository): AndroidViewModel(app) {

    val isLoading = ObservableField(false)
    val users = MutableLiveData<List<User>>()

    private val disposables = CompositeDisposable()


    fun getUsers() {
        isLoading.set(true)
        disposables.add(usersRepository.getUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onError = { error ->
                    Timber.e(error, "Error while getting users")
                    isLoading.set(false)
                }, onSuccess = { users ->
                    isLoading.set(false)
                    this.users.value = users
                }))
    }

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }
}