package example.com.testgallery.di

import example.com.testgallery.App
import example.com.testgallery.BuildConfig
import example.com.testgallery.data.network.IApi
import example.com.testgallery.data.repositories.PhotosRepository
import example.com.testgallery.data.repositories.UsersRepository
import example.com.testgallery.ui.albums.AlbumsAdapter
import example.com.testgallery.ui.albums.AlbumsViewModelFactory
import example.com.testgallery.ui.photos.PhotosAdapter
import example.com.testgallery.ui.photos.PhotosViewModelFactory
import example.com.testgallery.ui.users.UsersAdapter
import example.com.testgallery.ui.users.UsersViewModelFactory
import org.koin.android.module.AndroidModule
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class AppModule(private val app: App) : AndroidModule() {

    override fun context() = applicationContext {
        provide { app }
        provide { createApiClient() }
        provide { UsersRepository(get()) }
        provide { PhotosRepository(get()) }

        context(name = "UsersActivity") {
            provide { UsersViewModelFactory(get(), get()) }
            provide { UsersAdapter(get()) }
        }
        context(name = "AlbumsActivity") {
            provide { AlbumsViewModelFactory(get(), get()) }
            provide { AlbumsAdapter(get()) }
        }
        context(name = "PhotosActivity") {
            provide { PhotosViewModelFactory(get(), get()) }
            provide { PhotosAdapter(get()) }
        }
        context(name = "PreviewActivity") {
        }
    }

    private fun createApiClient(): IApi {
        val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build()

        return retrofit.create(IApi::class.java)
    }
}