package example.com.testgallery

import android.app.Application
import com.github.bskierys.pine.Pine
import example.com.testgallery.di.AppModule
import org.koin.android.ext.android.startAndroidContext
import timber.log.Timber

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Pine.growDefault())
        }
        startAndroidContext(this, AppModule(this))
    }
}