package example.com.testgallery.data.repositories

import example.com.testgallery.data.network.IApi

class UsersRepository(private val api: IApi) {

    fun getUsers() = api.getUsers()

    fun getUserAlbums(userId: Int) = api.getUserAlbums(userId)
}