package example.com.testgallery.data.models

data class Geo(val lat: String, val lng: String)