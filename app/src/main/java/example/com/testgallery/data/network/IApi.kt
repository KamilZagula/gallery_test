package example.com.testgallery.data.network

import example.com.testgallery.data.models.Album
import example.com.testgallery.data.models.Photo
import example.com.testgallery.data.models.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface IApi {
    @GET("/users")
    fun getUsers(): Single<List<User>>

    @GET("/albums")
    fun getUserAlbums(@Query("userId") userId: Int): Single<List<Album>>

    @GET("/photos")
    fun getPhotosFromAlbum(@Query("albumId") albumId: Int): Single<List<Photo>>
}