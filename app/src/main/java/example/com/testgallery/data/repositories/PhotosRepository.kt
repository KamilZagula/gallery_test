package example.com.testgallery.data.repositories

import example.com.testgallery.data.network.IApi

class PhotosRepository(private val api: IApi) {

    fun getPhotosFromAlbum(albumId: Int) = api.getPhotosFromAlbum(albumId)
}