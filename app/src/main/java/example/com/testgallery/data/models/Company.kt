package example.com.testgallery.data.models

data class Company(val name: String, val catchPhrase: String, val bs: String)