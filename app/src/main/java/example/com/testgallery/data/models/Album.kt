package example.com.testgallery.data.models

data class Album(val userId: Int, val id: Int, val title: String)